# Raspberry PI Configuration

Add to /etc/modules

```
sudo nano /boot/config.txt
```

Insert in /boot/config.txt

```
[...]
dtoverlay=w1-gpio,gpiopin=4
[...]
```


# Project installation

Grab the code, unpack it and install dependencies


``` 
wget -O package.zip https://gitlab.com/pmoprea/temp-monitor-pi/repository/archive.tar.gz?ref=v0.1
tar -xvf temp-monitor-pi.tar.gz
cd temp-monitor-pi
pip install -r requirements.txt
```

## Configuration

For security reasons the script doesn't include any raw connection details. Instead, it uses environment variables which you need to configure locally.

## Run in console
Edit ```start.sh```, uncomment and configure the appropriate values for 

* EMON_WRITE_KEY - Self explanatory
* THINGSPEAK_CHANNEL - Self explanatory
* THINGSPEAK_WRITE_KEY - Self explanatory
* LOG_FILE_NAME - Not specifying a location for the log file will just output logging messages to console

If you don't set the API keys, the communication with the respective services will be disabled. The program will still run and output sensor information to the LCD display. Consider this as an option to test before publishing.



```
sh start.sh
```

## Set to run as a service

Move the environment variables from ``` start.sh ``` to ``` etc/systemd/temp_monitor_pi.conf ```. 

Edit ``` start.sh ``` and comment them out from there.
```
# export EMON_WRITE_KEY=
[...]
```

You then need to install two files in their respective locations:
* lib/systemd/system/temp_monitor_pi.service
* etc/systemd/temp_monitor_pi.conf

You copy the service file and you symlink the cofiguration file
```
sudo cp lib/systemd/system/temp_monitor_pi.service /lib/systemd/system/
sudo ln -s /home/pi/Projects/temp_monitor/etc/systemd/temp_monitor_pi.conf /etc/systemd/temp_monitor_pi.conf
```

Last, but not least, enable the service (this will make it run on boot)
```
sudo systemctl enable temp_monitor_pi.service
```

And run it
```
sudo service temp_monitor_pi start
```

Or stop it

```
sudo service temp_monitor_pi stop
```

Or restart it
```
sudo service temp_monitor_pi restart
```