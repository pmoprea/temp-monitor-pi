import os, time
import logging
import random
import json
from six.moves import urllib
from w1thermsensor import W1ThermSensor
from RPLCD import CharLCD
import RPi.GPIO as GPIO
import thingspeak

if not os.environ.get("LOG_FILE_NAME") or not os.environ.get("FULL_PATH_TO"):
    logging.error("LOG_FILE_NAME or FULL_PATH_TO environment variables not set. \
                    File logging is disabled. Please check your configuration file \
                    in \etc\systemd\ ")

log_file = ''.join([os.environ.get("FULL_PATH_TO"),os.environ.get("LOG_FILE_NAME")])
logging.basicConfig(filename=log_file,
                    format='%(levelname)s:%(message)s', 
                    level=logging.DEBUG)

# Configurations

# Lista de sensori interna
RUN_EVERY_SECONDS = 15

SENSOR_MAPPINGS = {
    'IN1': '0000067a5856',
    'EXT1': '000007516168',
}

SENSOR_CALIBRATIONS = {
    'IN1': -1,
    'EXT1': -2
}

HEATCONTROL_SENSOR = 'IN1' # Valoarea din SENSOR_MAPPINGS
HEAT_ON = 6 # Temperatura minima de la care porneste caldura
HEAT_OFF = 10 # Temperatura maxima de la care se opreste caldura
HEAT_STATUS = None # Daca e sau nu pornita caldura


# EMON
# Maparea senzorilor la denumirile EMON
EMON_ENABLED = True
EMON_SENSOR_MAPPINGS = {
    'InteriorTemperature1':'IN1',
    'ExteriorTemperature1':'EXT1',
}

emoncms_url = "https://emoncms.org"

emon_write_key=os.environ.get("EMON_WRITE_KEY")
if not os.environ.get("EMON_WRITE_KEY"):
    logging.error("EMON_WRITE_KEY environment variable not set. Emoncms.org is disabled")
    EMON_ENABLED=False



# ThingSpeak
# Maparea senzorilor la denumirile ThingSpeak
THINGSPEAK_ENABLED = True
THINGSPEAK_CHANNEL_MAPPINGS = {
    '1': 'IN1',
    '2': 'EXT1',
}


if not os.environ.get("THINGSPEAK_CHANNEL") or not os.environ.get("THINGSPEAK_WRITE_KEY"):
    logging.error("THINGSPEAK_CHANNEL or THINGSPEAK_WRITE_KEY environment variables not set. ThingSpeak is disabled")
    THINGSPEAK_ENABLED = False

thingspeak_channel = os.environ.get("THINGSPEAK_CHANNEL")
thingspeak_write_key = os.environ.get("THINGSPEAK_WRITE_KEY")

# # Functions needed for heating control
def setGPIOPinMode(pin, mode):
    GPIO.setup(pin, GPIO.OUT)
    GPIO.output(pin, mode)
    return

def heat_on():
    setGPIOPinMode(12, GPIO.LOW)

def heat_off():
    setGPIOPinMode(12, GPIO.HIGH)


def heatcontrol(readings):
    for reading_name, reading_value in readings.items():
        if get_dict_key_by_value(SENSOR_MAPPINGS, reading_name)==HEATCONTROL_SENSOR:
            temp = reading_value
            if temp<=HEAT_ON and not HEAT_STATUS:
                heat_on()
                return True
            if temp>=HEAT_OFF and HEAT_STATUS:
                heat_off()
                return False
        


def write_to_lcd(readings=[]):
    lcd = CharLCD(cols=20, rows=4, pin_rs=11, pin_e=13, pins_data=[29, 31, 33, 35])
    lcd.cursor_pos = (0, 0)
    lcd.write_string("%s" %time.strftime("%H:%M"))
    lcd.cursor_pos = (0, 10)
    lcd.write_string("%s" %time.strftime("%m/%d/%Y"))
    lcd.cursor_pos = (1, 6)
    lcd.write_string("HORTIPROD")
    lcd.cursor_pos = (2, 3)
    lcd.write_string("IN1   %s " % ("ON" if HEAT_STATUS else "OFF"))
    lcd.cursor_pos = (2, 13)
    lcd.write_string("EXT1")
    lcd.cursor_pos = (3, 1)
    dict_readings = {}
    for reading_name, reading_value in readings.items():
        logging.debug("Sensor %s reads %.2f deg C" % (reading_name,
                                                        reading_value))
    lcd.write_string (str(readings["IN1"]) + " C  ")
    lcd.cursor_pos = (3, 12)
    lcd.write_string (str(readings["EXT1"]) + " C  ") 



def get_dict_key_by_value(dict={}, lookup=None):
    try:
        for key, value in dict.items():
            if value==lookup:
                return key
    except:
        return None

# functions
def read_sensors():
    result = {}
    try:
        for sensor in W1ThermSensor.get_available_sensors():
            sensor_name = get_dict_key_by_value(SENSOR_MAPPINGS, sensor.id)
            read_temperature = sensor.get_temperature()
            read_temperature+=SENSOR_CALIBRATIONS[sensor_name]
            result[sensor_name] = round(read_temperature, 2)
            logging.debug("Read from sensor %s(%s): %s" % (sensor_name, sensor.id, read_temperature))
    except:
        pass
    return result

def post_to_emoncms(readings=[]):
    if not EMON_ENABLED:
        return
    post_dict = {}
    logging.debug("Posting to EmonCMS... ")
    for reading_name, reading_value in readings.items():
        try:
            post_dict[get_dict_key_by_value(EMON_SENSOR_MAPPINGS,reading_name)] = reading_value
        except:
            pass

    if post_dict:
        url = ''.join ([
                    emoncms_url,"/input/post.json?node=1&apikey=",
                    emon_write_key,
                    "&json=",
                    urllib.parse.quote(json.dumps(post_dict))
                   ])
        try:
            response = urllib.request.urlopen(url)
            logging.debug(url)
            logging.debug("Server responded: HTTP%s: %s" % (response.code,response.read()))
        except:
            logging.debug("Error transmitting information to EmonCMS server")
    else:
        logging.debug("No payload to send to server")

def post_to_thingspeak(readings=[]):
    if not THINGSPEAK_ENABLED:
        return
    logging.debug("Posting to ThingSpeak... ")
    post_dict = {}
    try:
        channel = thingspeak.Channel(id=thingspeak_channel,write_key=thingspeak_write_key)
    except:
        logging.debug("Unable to retrieve channel. Please check your keys.")
        return

    try:
        for reading_name, reading_value in readings.items():
            post_dict[get_dict_key_by_value(THINGSPEAK_CHANNEL_MAPPINGS,reading_name)] = reading_value
        logging.debug(post_dict)
        response = channel.update(post_dict)
        logging.debug("Server responded: HTTP%s:" % response)
    except:
        logging.debug("Error transmitting information to ThingSpeak server.")


# run baby run
def run():
    logging.debug("Reading sensors...")
    readings = read_sensors()
    logging.debug("Complete readings: %s" % readings)
    HEAT_STATUS = heatcontrol(readings)
    write_to_lcd(readings)
    post_to_emoncms(readings)
    post_to_thingspeak(readings)


if __name__ == "__main__":
    while True:
        run()
        time.sleep(RUN_EVERY_SECONDS)

